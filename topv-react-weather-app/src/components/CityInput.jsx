import React, { useState } from 'react';
import './CityInput.css';

const CityInput = (props) => {
  const [cityInput, setCityInput] = useState('');
  const [loading, setLoading] = useState(false);
  const [placeholderText, setPlaceholderText] = useState('Enter a City...');

  const onKlickHandler = async (e) => {
    e.persist();
    const eventKey = e.which ? e.which : e.keyCode;
    const city = e.target.value;

    if (eventKey === 13) {
      if (/^[a-zA-ZäöüÄÖÜß ;.-]+$/.test(city)) {
        setLoading(true);
        const apiCallSuccess = await props.makeApiCall(city);
        setLoading(false);

        if (apiCallSuccess) {
          setCityInput(''); // Xóa văn bản trong trường input
          setPlaceholderText('Enter a City...'); // Reset placeholder
        } else {
          setCityInput(''); // Xóa văn bản trong trường input
          setPlaceholderText('City was not found, try again...');
        }
      } else {
        setCityInput(''); // Xóa văn bản trong trường input
        setPlaceholderText('Please enter a valid city name...');
      }
    }
  };

  const style = {
    top: props.city ? '-350px' : '-20px',
    width: '600px',
    display: 'inline-block',
    padding: '10px 0px 10px 30px',
    lineHeight: '120%',
    position: 'relative',
    borderRadius: '20px',
    outline: 'none',
    fontSize: '20px',
    transition: 'all 0.5s ease-out',
  };

  return (
    <input
      className={loading ? 'city-input loading' : 'city-input'}
      style={style}
      type='text'
      placeholder={placeholderText}
      value={cityInput}
      onChange={(e) => setCityInput(e.target.value)}
      onKeyPress={onKlickHandler}
    />
  );
};

export default CityInput;
