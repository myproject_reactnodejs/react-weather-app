import React, { useState, useEffect } from 'react';
import './WeatherBox.css';

const WeatherBox = ({ date, icon, temp }) => {
  // Khởi tạo biến state để lưu trữ đường dẫn của hình ảnh
  const [imageSrc, setImageSrc] = useState(icon ? `../images/${icon}.svg` : '../images/01d.svg');

  // Hàm để cập nhật đường dẫn của hình ảnh khi có sự thay đổi trong icon
  useEffect(() => {
    const newSrc = icon ? `../images/${icon}.svg` : '../images/01d.svg';
    setImageSrc(newSrc);
  }, [icon]);

  // Hàm trả về ngày trong tuần dựa vào giá trị Date được cung cấp
  const getDay = (date) => {
    const weekday = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    return weekday[new Date(date).getDay()];
  };

  return (
    <div className='weather-box'>
      {/* Hiển thị ngày trong tuần nếu có */}
      <h1>{date ? getDay(date) : ''}</h1>
      {/* Hiển thị biểu tượng thời tiết */}
      <img
        src={imageSrc}
        alt='sun'
      />

      {/* Hiển thị nhiệt độ */}
      <span className='temp'>{Math.round(temp - 273.15)}°C</span>
    </div>
  );
};

export default WeatherBox;
