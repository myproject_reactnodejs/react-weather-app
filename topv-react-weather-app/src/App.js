import React, { useState } from 'react';
import './App.css';
import MainWeatherWindow from './components/MainWeatherWindow';
import CityInput from './components/CityInput';
import WeatherBox from './components/WeatherBox';

const App = () => {
  // Sử dụng useState hooks để quản lý trạng thái của thành phố và dữ liệu thời tiết cho 5 ngày
  const [city, setCity] = useState(undefined);
  const [days, setDays] = useState(new Array(5));

  // Hàm cập nhật state với dữ liệu mới từ API
  const updateState = (data) => {
    const city = data.city.name;
    const newDays = [];

    // Lấy chỉ số của các ngày tiếp theo từ dữ liệu API
    const dayIndices = getDayIndices(data);

    // Tạo objects cho mỗi ngày và cập nhật state
    for (let i = 0; i < 5; i++) {
      newDays.push({
        date: data.list[dayIndices[i]].dt_txt,
        weather_desc: data.list[dayIndices[i]].weather[0].description,
        icon: data.list[dayIndices[i]].weather[0].icon,
        temp: data.list[dayIndices[i]].main.temp
      });
    }

    setCity(city);
    setDays(newDays);
  };

  // Hàm gọi API và xử lý dữ liệu trả về
  const makeApiCall = async (city) => {
    try {
      const response = await fetch(
        `https://api.openweathermap.org/data/2.5/forecast?q=${city}&appid=ba7a28b8db3b0982ca47c852dbeb79f7`
      );
      const api_data = await response.json();

      // Kiểm tra mã trạng thái của API và cập nhật state
      if (api_data.cod === '200') {
        updateState(api_data);
        return true;
      } else {
        return false;
      }
    } catch (error) {
      // Xử lý lỗi nếu gặp lỗi khi gọi API
      console.error('Error fetching data:', error);
      return false;
    }
  };

  // Hàm lấy chỉ số của các ngày tiếp theo trong dữ liệu API
  const getDayIndices = (data) => {
    const dayIndices = [];
    dayIndices.push(0);

    let index = 0;
    let tmp = data.list[index].dt_txt.slice(8, 10);

    for (let i = 0; i < 4; i++) {
      while (
        tmp === data.list[index].dt_txt.slice(8, 10) ||
        data.list[index].dt_txt.slice(11, 13) !== '15'
      ) {
        index++;
      }
      dayIndices.push(index);
      tmp = data.list[index].dt_txt.slice(8, 10);
    }

    return dayIndices;
  };

  // Tạo các thành phần WeatherBox cho 4 ngày tiếp theo từ dữ liệu thời tiết
  const weatherBoxes = days.slice(1).map((day, index) => (
    <li key={index}>
      <WeatherBox {...day} />
    </li>
  ));

  return (
    <div className='App'>
      <header className='App-header'>
        {/* Hiển thị MainWeatherWindow với thành phố và dữ liệu thời tiết */}
        <MainWeatherWindow data={days[0]} city={city}>
          {/* Hiển thị CityInput và danh sách WeatherBox */}
          <CityInput city={city} makeApiCall={makeApiCall} />
          <ul className='weather-box-list'>{weatherBoxes}</ul>
        </MainWeatherWindow>
      </header>
    </div>
  );
};

export default App;
